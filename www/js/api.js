var Api = {
	
	endpoint: "http://rodrigomac.local:3000/",
	
	get: function(url, success, error){
		$.mobile.loading('show');
		$.ajax({
			type: "GET",
			dataType: "json",
			url: Api.endpoint + url,
			cache: false,
			crossDomain: true,
			xhrFields: {
				withCredentials: true
			},
			success: function(data){
				$.mobile.loading('hide');
				if(success)success(data);
			},
			error: function(data){
				$.mobile.loading('hide');
				if(error)error(data);
			}
		})
	},
	post: function(url, data, success, error){
		$.mobile.loading('show');
		$.ajax({
			type: "POST",
			dataType: "json",
			url: Api.endpoint + url,
			data: data,
			cache: false,
			crossDomain: true,
			xhrFields: {
				withCredentials: true
			},
			success: function(data){
				$.mobile.loading('hide');
				if(success)success(data);
			},
			error: function(data){
				$.mobile.loading('hide');
				if(error)error(data);
			}
		})
	}
}