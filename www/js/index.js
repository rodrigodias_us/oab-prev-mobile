angular.module('app', ['ui.utils'])

.controller('AppController', function($scope){
	
	$scope.root = "#index";
	
	// Troca de Estados
	$scope.logado = function(){
		$scope.root = "#meu-plano"
	}
	$scope.deslogado = function(){
		$scope.root = "#index"
	}
	$scope.visitante = function(){
		$scope.root = "#area-visitante"
	}
	
	// Ações
	$scope.login = function(){
		Api.get('participantes/sign_out', function(){
			$scope.deslogado();
			Api.post("participantes/sign_in", {
				"participante":{"cpf": $scope.cpf, "password": $scope.password}
			}, function(data){
				$.mobile.navigate("#meu-plano");
				$scope.cpf = "";
				$scope.password = "";
				$scope.$apply();
				$scope.logado();
				console.log('logou:' + data);
			}, function(){
				navigator.notification.alert('CPF ou senha inválidos', null, 'Atenção', 'OK');
				console.log('Error');
			});
		});
	}
	$scope.me = function(){
		Api.get('api/me', function(data){
			console.log(data);
		})
	}
	$scope.goHome = function(){
		$.mobile.navigate($scope.root);
	}
	
	$scope.init = function(){
		Api.get('api/me', function(){
			$scope.logado();
			$scope.goHome();
			navigator.splashscreen.hide();
		}, function(){
			$scope.deslogado();
			$scope.goHome();
			navigator.splashscreen.hide();
		});
		
		$('[data-role="header"]').click($scope.goHome);
	}
	$scope.init();
});